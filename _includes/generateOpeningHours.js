<script>
var today = new Date();

var openingHours = [ 
    {%- for day in page.openingHours -%}
    [{%- if day == null -%}
        null, null
        {%- else -%}
        new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.openHour }}, {{ day.openMinute }}, 0, 0),
        new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.closeHour }}, {{ day.closeMinute }}, 0, 0)
        {%- endif -%}
    ],
    {%- endfor -%}
];
</script>
