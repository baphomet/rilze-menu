<script>
function getCurrentOpeningHoursState(today, todayOpeningHours) {
    var state = false;

    if (todayOpeningHours[0] != null && todayOpeningHours[1] != null) {
        if (today >= todayOpeningHours[0] && today <= todayOpeningHours[1]) {
            state = true;
        }
    }

    return state;
}
</script>
