---
layout: page
permalink: /specialities/
title: Spezialitäten
---
{% for speciality in site.specialities[page.specialities] %}
  {{ speciality }}
{% endfor %}
