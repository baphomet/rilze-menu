---
layout: page
title: Impressum
permalink: /imprint
---

## Impressum

**Eigentümer und Herausgeber**  
M. Nebel und B. Zingler <br>
Südring 49 <br>
76761 R&uuml;lzheim  

Anfragen, Kontakt oder Feedback bitte ausschließlich über kontakt[at]rilze-menu[dot]de.

**Rechtliche Hinweise:**  
Rilze-Menu prüft und aktualisiert die Informationen auf seinen Webseiten ständig. Trotz aller Sorgfalt können sich die Daten inzwischen verändert haben. Eine Haftung oder Garantie für die Aktualität, Richtigkeit und Vollständigkeit der zur Verfügung gestellten Informationen kann daher nicht übernommen werden. Gleiches gilt auch für alle anderen Webseiten, auf die mittels Hyperlink verwiesen wird. Rilze-Menu ist für den Inhalt der Webseiten, die aufgrund einer solchen Verbindung erreicht werden, nicht verantwortlich.


Des Weiteren behält sich Rilze-Menu das Recht vor, Änderungen oder Ergänzungen der bereitgestellten Informationen vorzunehmen, sowie den Dienst ohne Ankündigung offline zu schalten. Trotz häufigem Updateintervall übernimmt Rilze-Menu keine Haftung für Datenverlust.


Inhalt und Struktur der Webseiten von Rilze-Menu sind urheberrechtlich geschützt. Die Vervielfältigung von Informationen oder Daten, insbesondere die Verwendung von Texten, Textteilen oder Bildmaterial, bedarf der vorherigen Zustimmung von Rilze-Menu.


Copyright © 2019-{{ site.time | date: '%Y' }} Rilze-Menu. Alle Rechte vorbehalten.
