---
layout: page
permalink: /index
description: > # this means to ignore newlines
  <strong>Rilze Men&uuml; ist der Online-Speiseplan von R&uuml;lzheimern für R&uuml;lzheim und Umgebung!</strong><br />
  Du hast Hunger, keine Lust zu kochen und möchtest lieber etwas bestellen? Dann bist du hier genau richtig!<br />
  Alle Restaurants und Speisekarten aus der Verbandsgemeinde Rülzheim und Umgebung. Sowohl Heim- und Lieferservice, als auch für Selbstabholer.<br />
  Wir geben uns Mühe, die Webseite auf dem neusten Stand zu halten und regelmäßig zu aktualisieren. Dabei freuen uns natürlich über eure Mithilfe.<br />
description_title: "Rilze-Menu.de"
---
<table class="table table-striped table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Restaurant</th>
      <th scope="col">Liefert?</th>
      <th scope="col">Ort</th>
      <th scope="col" class="d-none d-sm-table-cell">Spezialit&auml;t</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
{%- for page in site.pages -%}
  {%- if page.categories contains 'restaurant' -%}
    <tr>
        <td scope="row"><a href="{{ page.url | absolute_url }}" class="stretched-link">{{page.title}}</a></td>
        <td>
          {%- if page.deliver == true -%}
            <i class="fa fa-check" style="color: green;" aria-hidden="true"></i>
          {%- else -%}
            <i class="fa fa-times" style="color: red;" aria-hidden="true"></i>
          {%- endif -%}
        </td>
        <td>{{page.village}}</td>
        <td class="d-none d-sm-table-cell">{{page.speciality}}</td>
        <td id="{{ page.path | slugify | replace: '-', '_' }}_openHoursCurrentState"></td>
    </tr>
  {%- endif -%}
{%- endfor -%}
  </tbody>
</table>

{%- include getCurrentOpeningHoursState.js -%}
<script>
function setOpeningHoursState(elementId, state) {
  document.getElementById(elementId).innerHTML = (state ? "Geöffnet!": "Geschlossen!");
  document.getElementById(elementId).style.color = (state ? "green" : "red");
}

var today = new Date();
var weekDay = today.getDay();

{%- for page in site.pages -%}
  {%- if page.categories contains 'restaurant' -%}
      {%- assign restaurant_name = page.path | slugify | replace: '-', '_'  -%}
      var {{ restaurant_name }}_openingHours = [ 
        {%- for day in page.openingHours -%}
        [
          {%- if day == null -%}
            null, null
          {%- else -%}
          new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.openHour }}, {{ day.openMinute }}, 0, 0),
          new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.closeHour }}, {{ day.closeMinute }}, 0, 0)
          {%- endif -%}
        ],
        {%- endfor -%}
      ];
      var {{ restaurant_name }}_state = getCurrentOpeningHoursState(today, {{ restaurant_name }}_openingHours[weekDay]);
      setOpeningHoursState("{{ restaurant_name }}_openHoursCurrentState", {{ restaurant_name }}_state);
  {%- endif -%}
{%- endfor -%}
</script>
