---
layout: page
title: Partner
permalink: /partner
description_title: Partner
description: >
  <strong>Nehmen Sie Kontakt mit Rilze-Menu.de auf!</strong><br />
  <br />
  Ist Ihr Lieblings-Lokal oder das eigene Restaurant nicht auf der Webseite zu finden? Kontaktieren Sie uns! </br>
  Wir sind st&auml;ndig auf der Suche nach neuen Partnern, Infos und Speisekarten. Falls Sie Material aus dem Bereich der Essenskultur und Gastronomie in R&uuml;lzheim oder der Umgebung anzubieten haben, schreiben Sie uns via E-Mail kontakt[at]rilze-menu[dot]de. </br>
  Auch für Fragen, Anmerkungenund Verbesserungsvorschl&auml;ge sid wir stets offen. Wir freuen uns auf Ihre Mithilfe?
---
