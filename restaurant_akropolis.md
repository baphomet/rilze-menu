---
layout: restaurant
categories: [restaurant]
title: Akropolis
permalink: /akropolis
specialities: [Griechisch]
speciality: "Griechisch"
village: Kuhardt
deliver: false
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 24, closeMinute: 0 } # sunday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # monday
 - null # tuesday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # wednesday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # thursday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # friday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 24, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Restaurant Akropolis</dd>
    <dd>Tel: <a href="tel:+497272932699">07272 932699</a></dd>
    <dd>Gartenstraße 22</dd>
    <dd>76773 Kuhardt</dd>
    <dd><a href="https://goo.gl/maps/KVhVMhSfKYD54aQd6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/akropolis/Akropolis_Speisekarte_6.jpg' | relative_url }} "Seite 6"){: .menu-card}

