---
layout: restaurant
categories: [restaurant]
title: Andys Pinte
permalink: /andys-pinte
specialities: [Deutsch]
speciality: "Deutsch"
village: Herxheim
deliver: false
openingHours:
 - null #sunday 
 - { openHour: 17, openMinute: 00, closeHour: 21, closeMinute: 00 } # monday
 - { openHour: 17, openMinute: 00, closeHour: 21, closeMinute: 00 } # tuesday
 - { openHour: 17, openMinute: 00, closeHour: 21, closeMinute: 00 } # wednesday
 - { openHour: 17, openMinute: 00, closeHour: 21, closeMinute: 00 } # thursday
 - { openHour: 17, openMinute: 00, closeHour: 21, closeMinute: 00 } # friday
 - null # saturday
 - null # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Andys </dd>
    <dd>Tel: <a href="tel:+4972767688">07276 7688</a></dd>
    <dd>Lehrgasse 1</dd>
    <dd>76863 Herxheim</dd>
    <dd><a href="https://g.page/Andys-Pinte?share" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/andys_pinte/andys_pinte.jpg' | relative_url }} "Seite 1"){: .menu-card}


