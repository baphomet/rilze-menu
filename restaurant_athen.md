---
layout: restaurant
categories: [restaurant]
title: ATHEN
permalink: /athen
specialities: [Griechisch]
speciality: "Griechisch"
village: R&uuml;lzheim
deliver: false
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # sunday
 - null                                                            # monday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # tuesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # thursday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Restaurant ATHEN</dd>
    <dd>Tel: <a href="tel:+4972729723980">07272 9723980</a></dd>
    <dd>Bahnhofstraße 6</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/tHNUZGGM4kTarArf6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/athen/athen-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/athen/athen-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/athen/athen-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/athen/athen-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/athen/athen-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/athen/athen-6.jpg' | relative_url }} "Seite 6"){: .menu-card}
