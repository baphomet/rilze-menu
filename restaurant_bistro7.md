---
layout: restaurant
categories: [restaurant]
title: Bistro 7
permalink: /bistro7
specialities: [Deutsch]
speciality: "Deutsch"
village: H&ouml;rdt
deliver: true
openingHours:
 - null                                                            # sunday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 } # monday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 } # tuesday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 }  # wednesday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 }  # thursday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 }  # friday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 }  # saturday
 - { openHour: 18, openMinute: 0, closeHour: 24, closeMinute: 0 }  # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Bistro 7</dd>
    <dd>Tel: <a href="tel:+4915110013816">01511 0013816</a></dd>
    <dd>Schulzenstraße 24</dd>
    <dd>76771 H&ouml;rdt</dd>
    <dd><a href="https://goo.gl/maps/dgQ5j6GJUEUXbPur8" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/bistro7/bistro7-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/bistro7/bistro7-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/bistro7/bistro7-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

