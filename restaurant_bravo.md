---
layout: restaurant
categories: [restaurant]
title: Bravo
permalink: /bravo
specialities: [Italienisch]
speciality: "Italienisch"
village: R&uuml;lzheim
deliver: true
minimum_deliver_order: "12+ (siehe Karte)"
openingHours:
 - { openHour: 11, openMinute: 0, closeHour: 23, closeMinute: 0 } # sunday
 - null                                                            # monday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # tuesday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # wednesday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 11, openMinute: 0, closeHour: 23, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 0, closeHour: 23, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 0, closeHour: 23, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Pizzeria Bravo</dd>
    <dd>Tel: <a href="tel:+497272774295">07272 774295</a></dd>
    <dd>WhatsApp: <a href="https://wa.me/4915211469739">0152 11469739</a></dd>
    <dd>Alte M&uuml;hlgasse 15</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/RVs4FgHx8gGy4EMH6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/bravo/bravo-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/bravo/bravo-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/bravo/bravo-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/bravo/bravo-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/bravo/bravo-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/bravo/bravo-6.jpg' | relative_url }} "Seite 6"){: .menu-card}
