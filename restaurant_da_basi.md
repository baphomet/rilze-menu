---
layout: restaurant
categories: [restaurant]
title: Da Basi
permalink: /da-basi
specialities: [Italienisch]
speciality: "Italienisch"
village: R&uuml;lzheim
deliver: true
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 20, closeMinute: 30 } # sunday
 - null                                                            # monday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # tuesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Da Basi</dd>
    <dd>Tel: <a href="tel:+497272774579">07272 774579</a></dd>
    <dd>Am See</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/DK22uMV1PjBLWcmc8" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/da_basi/da-basi-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/da_basi/da-basi-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/da_basi/da-basi-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/da_basi/da-basi-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/da_basi/da-basi-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/da_basi/da-basi-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 67]({{ '/assets/images/restaurants/da_basi/da-basi-7.jpg' | relative_url }} "Seite 7"){: .menu-card}

![Seite 8]({{ '/assets/images/restaurants/da_basi/da-basi-8.jpg' | relative_url }} "Seite 8"){: .menu-card}



