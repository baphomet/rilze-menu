---
layout: restaurant
categories: [restaurant]
title: Da Chicca
permalink: /da-chicca
specialities: [Italienisch]
speciality: "Italienisch"
village: R&uuml;lzheim
deliver: true
openingHours:
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } #sunday
 - null # monday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } #tuesday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } # wednesday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } # friday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } # saturday
 - { openHour: 17, openMinute: 0, closeHour: 22, closeMinute: 30 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Da Chicca</dd>
    <dd>Tel: <a href="tel:+4972727000034">07272 7000034</a></dd>
    <dd>Bismarckstraße 2</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/fE3Q2JzCnxkri9ET8" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/da_chicca/da-chicca-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/da_chicca/da-chicca-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/da_chicca/da-chicca-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/da_chicca/da-chicca-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/da_chicca/da-chicca-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/da_chicca/da-chicca-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

