---
layout: restaurant
categories: [restaurant]
title: Da Marino
permalink: /da-marino
specialities: [Italienisch]
speciality: "Italienisch"
village: Kuhardt
deliver: false
openingHours:
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # sunday
 - { openHour: 17, openMinute: 0, closeHour: 23, closeMinute: 0 }  # monday
 - { openHour: 17, openMinute: 0, closeHour: 23, closeMinute: 0 } # tuesday
 - null                                                           # wednesday
 - { openHour: 17, openMinute: 0, closeHour: 23, closeMinute: 0 } # thursday
 - { openHour: 17, openMinute: 0, closeHour: 23, closeMinute: 0 } # friday
 - { openHour: 17, openMinute: 0, closeHour: 23, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Da Marino</dd>
    <dd>Tel: <a href="tel:+497272750000">07272 750000</a></dd>
    <dd>Am Rheinberg 13</dd>
    <dd>76773 Kuhardt</dd>
    <dd><a href="https://goo.gl/maps/mjBiRtWp8wLrNaVX9" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/da_marino/DaMarino_1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/da_marino/DaMarino_2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/da_marino/DaMarino_3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/da_marino/DaMarino_4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/da_marino/DaMarino_5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/daa_marino/DaMarino_6.jpg' | relative_url }} "Seite 6"){: .menu-card}




