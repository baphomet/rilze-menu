---
layout: restaurant
categories: [restaurant]
title: Diverso
permalink: /diverso
specialities: [Italienisch]
speciality: "Italienisch"
village:  W&ouml;rth am Rhein
deliver: true
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # sunday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # monday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # tuesday
 - null                                                             # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Diverso</dd>
    <dd>Tel: <a href="tel:+497271922092">07271 922092</a></dd>
    <dd>Ottstraße 23A</dd>
    <dd>76744 Wörth am Rhein</dd>
    <dd><a href="https://goo.gl/maps/PRiMrLTiyzi8o2b79" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/diverso/diverso-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/diverso/diverso-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/diverso/diverso-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/diverso/diverso-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/diverso/diverso-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/diverso/diverso-6.jpg' | relative_url }} "Seite 6"){: .menu-card}





