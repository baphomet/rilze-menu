---
layout: restaurant
categories: [restaurant]
title: Galerie
permalink: /galerie
specialities: [Italienisch]
speciality: "Italienisch"
village: Herxheim
deliver: false
openingHours:
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } #sunday 
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } # monday
 - null # tuesday
 - null # wednesday
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } # thursday
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } # friday
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } # saturday
 - { openHour: 10, openMinute: 00, closeHour: 24, closeMinute: 00 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Café Restaurant Galerie</dd>
    <dd>Tel: <a href="tel:+497276914155">07276 914155</a></dd>
    <dd>Oberhohlstraße 5</dd>
    <dd>76863 Herxheim</dd>
    <dd><a href="https://goo.gl/maps/NXd4BWhJCuxp5rt26" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/galerie/galerie-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/galerie/galerie-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/galerie/galerie-3.jpg' | relative_url }} "Seite 3"){: .menu-card}




