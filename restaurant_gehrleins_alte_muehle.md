---
layout: restaurant
categories: [restaurant]
title: Gehrlein's Alte Muehle
permalink: /gehrleins-alte-muehle
specialities: [Deutsch]
speciality: "Deutsch"
village:  Hatzenb&uuml;hl
deliver: true
openingHours:
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 0 } # sunday
 - null                                                            # monday
 - null                                                            # tuesday
 - { openHour: 16, openMinute: 00, closeHour: 20, closeMinute: 0 }  # wednesday
 - { openHour: 16, openMinute: 00, closeHour: 20, closeMinute: 0 } # thursday
 - { openHour: 16, openMinute: 00, closeHour: 20, closeMinute: 0 }  # friday
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 0 } # saturday
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 0 }  # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Gehrlein's Alte M&uuml;ehle</dd>
    <dd>Tel: <a href="tel:+4972729579993">07272 9579993</a></dd>
    <dd>Zur Untermühle 1</dd>
    <dd>Am Ortsrand von Rheinzabern, 76770 Hatzenbühl</dd>
    <dd><a href="https://goo.gl/maps/jDQKLnFJxYH1LzweA" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/alte-muehle/alte-muehle-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/alte-muehle/alte-muehle-2.jpg' | relative_url }} "Seite 2"){: .menu-card}





