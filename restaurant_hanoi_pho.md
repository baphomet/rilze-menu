---
layout: restaurant
categories: [restaurant]
title: Hanoi Pho
permalink: /hanoi-pho
specialities: [Asiatisch]
speciality: "Asiatisch"
village: Bellheim
deliver: true
openingHours:
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # sunday
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # monday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # tuesday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # wednesday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # thursday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Hanoi Pho</dd>
    <dd>Tel: <a href="tel:+4972729738919">07272 9738919</a></dd>
    <dd>Hauptstraße 92</dd>
    <dd>76756 Bellheim</dd>
    <dd><a href="https://goo.gl/maps/1qDSt8BM49GuNJXG9" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas      fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-0.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-1.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-2.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-3.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-4.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-5.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 7]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-6.jpg' | relative_url }} "Seite 7"){: .menu-card}

![Seite 8]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-7.jpg' | relative_url }} "Seite 8"){: .menu-card}

![Seite 9]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-8.jpg' | relative_url }} "Seite 9"){: .menu-card}

![Seite 10]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-9.jpg' | relative_url }} "Seite 10"){: .menu-card}

![Seite 11]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-10.jpg' | relative_url }} "Seite 11"){: .menu-card}

![Seite 12]({{ '/assets/images/restaurants/hanoi_pho/hanoi-pho-11.jpg' | relative_url }} "Seite 12"){: .menu-card}
