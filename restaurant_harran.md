---
layout: restaurant
categories: [restaurant]
title: Harran Pizza & Grill Haus
permalink: /harran
specialities: [Tuerkisch]
speciality: "T&uuml;rkisch"
village: R&uuml;lzheim
deliver: true
minimum_deliver_order: 10
openingHours:
 - { openHour: 12, openMinute: 0, closeHour: 24, closeMinute: 0 } # sunday
 - { openHour: 11, openMinute: 0, closeHour: 24, closeMinute: 0 } # monday
 - { openHour: 11, openMinute: 0, closeHour: 24, closeMinute: 0 } # tuesday
 - { openHour: 11, openMinute: 0, closeHour: 24, closeMinute: 0 } # wednesday
 - { openHour: 11, openMinute: 0, closeHour: 24, closeMinute: 0 } # thursday
 - { openHour: 11, openMinute: 0, closeHour: 25, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 0, closeHour: 25, closeMinute: 0 } # saturday
 - { openHour: 12, openMinute: 0, closeHour: 24, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Harran Pizza & Grill Haus</dd>
    <dd>Tel: <a href="tel:+4972729739080">07272 9739080</a></dd>
    <dd>Neue Landstrasse 65</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/aSBGS4LXm3kQbqk56" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/harran/harran-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/harran/harran-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/harran/harran-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/harran/harran-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/harran/harran-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/harran/harran-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 7]({{ '/assets/images/restaurants/harran/harran-7.jpg' | relative_url }} "Seite 7"){: .menu-card}

![Seite 8]({{ '/assets/images/restaurants/harran/harran-8.jpg' | relative_url }} "Seite 8"){: .menu-card}
