---
layout: restaurant
categories: [restaurant]
title: Ischia
permalink: /ischia
specialities: [Italienisch]
speciality: "Italienisch"
village: Bellheim
deliver: false
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # sunday
 - null                                                           # monday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # tuesday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # thursday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 23, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Ischia Restaurante Pizzeria</dd>
    <dd>Tel: <a href="tel:+4972729297375">07272 9297375</a></dd>
    <dd>Zeiskamer Str. 20</dd>
    <dd>76756 Bellheim</dd>
    <dd><a href="https://goo.gl/maps/URmbCMmPFGYzg1sF7" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/ischia/ischia-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/ischia/ischia-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/ischia/ischia-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/ischia/ischia-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/ischia/ischia-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/ischia/ischia-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 7]({{ '/assets/images/restaurants/ischia/ischia-7.jpg' | relative_url }} "Seite 7"){: .menu-card}

![Seite 8]({{ '/assets/images/restaurants/ischia/ischia-8.jpg' | relative_url }} "Seite 8"){: .menu-card}

![Seite 9]({{ '/assets/images/restaurants/ischia/ischia-9.jpg' | relative_url }} "Seite 9"){: .menu-card}

![Seite 10]({{ '/assets/images/restaurants/ischia/ischia-10.jpg' | relative_url }} "Seite 10"){: .menu-card}

![Seite 11]({{ '/assets/images/restaurants/ischia/ischia-11.jpg' | relative_url }} "Seite 11"){: .menu-card}

