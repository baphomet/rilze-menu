---
layout: restaurant
categories: [restaurant]
title: Kaminstubb
permalink: /kaminstubb
specialities: [Deutsch]
speciality: "Deutsch"
village:  W&ouml;rth am Rhein
deliver: false
openingHours:
 - { openHour: 11, openMinute: 00, closeHour: 24, closeMinute: 0 } # sunday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 }  # monday
 - null                                                            # tuesday
 - null                                                            # wednesday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # thursday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # friday
 - { openHour: 17, openMinute: 00, closeHour: 24, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 00, closeHour: 24, closeMinute: 0 }  # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Kaminstubb</dd>
    <dd>Tel: <a href="tel:+4972714767">07271 4767</a></dd>
    <dd>Raiffeisenstraße 3</dd>
    <dd>76744 Wörth am Rhein</dd>
    <dd><a href="https://goo.gl/maps/GGBpGrtT3UJzm5UN7" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/kaminstubb/kaminstubb.jpg' | relative_url }} "Seite 1"){: .menu-card}




