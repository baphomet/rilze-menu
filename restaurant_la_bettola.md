---
layout: restaurant
categories: [restaurant]
title: La Bettola
permalink: /la-bettola
specialities: [Italienisch]
speciality: "Italienisch"
village: Herxheim
deliver: true
openingHours:
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } #sunday 
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # monday
 - null # tuesday
 - null # wednesday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # friday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 }# saturday
 - { openHour: 16, openMinute: 30, closeHour: 22, closeMinute: 30 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>La Bettola</dd>
    <dd>Tel: <a href="tel:+4972765711">07276 5711</a></dd>
    <dd>Poststraße 1</dd>
    <dd>76863 Herxheim</dd>
    <dd><a href="https://goo.gl/maps/o6HnSPN1GCfSQzCD6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/la_bettola/la_bettola_1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/la_bettola/la_bettola_2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/la_bettola/la_bettola_3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/la_bettola/la_bettola_4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/la_bettola/la_bettola_5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/la_bettola/la_bettola_6.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 7]({{ '/assets/images/restaurants/la_bettola/la_bettola_7.jpg' | relative_url }} "Seite 7"){: .menu-card}


