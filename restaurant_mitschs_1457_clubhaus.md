---
layout: restaurant
categories: [restaurant]
title: Mitsch's 1457 Clubhaus
permalink: /mitschs_1457_clubhaus
specialities: [Deutsch]
specialities: [Deutsch]
speciality: "Deutsch"
village: Herxheimweyher
deliver: true
openingHours:
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # sunday
 - { openHour: 16, openMinute: 0, closeHour: 23, closeMinute: 0 }  # monday
 - { openHour: 16, openMinute: 0, closeHour: 23, closeMinute: 0 } # tuesday
 - { openHour: 16, openMinute: 0, closeHour: 23, closeMinute: 0 }  # wednesday
 - { openHour: 16, openMinute: 0, closeHour: 23, closeMinute: 0 }  # thursday
 - { openHour: 16, openMinute: 0, closeHour: 23, closeMinute: 0 }  # friday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 }  # saturday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 }  # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Mitsch's 1457 Clubhaus</dd>
    <dd>Tel: <a href="tel:+4915164806806">015164806806</a></dd>
    <dd>Am Sportplatz 1</dd>
    <dd>76863 Herxheimweyher</dd>
    
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/mitschs_1457_clubhaus/mitschs-1457-clubhaus-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/mitschs_1457_clubhaus/mitschs-1457-clubhaus-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/mitschs_1457_clubhaus/mitschs-1457-clubhaus-3.jpg' | relative_url }} "Seite 3"){: .menu-card}




