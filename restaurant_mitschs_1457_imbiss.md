---
layout: restaurant
categories: [restaurant]
title: Mitsch's 1457 Imbiss
permalink: /mitschs_1457
specialities: [Deutsch]
speciality: "Deutsch"
village: Herxheim
deliver: false
openingHours:
 - null # sunday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # monday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # tuesday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # wednesday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # thursday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # friday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # saturday
 - { openHour: 11, openMinute: 0, closeHour: 20, closeMinute: 0 }  # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Mitsch`s 1457 Imbiss</dd>
    <dd>Tel: <a href="tel:+4915164806806">01516 4806806</a></dd>
    <dd>Gewerbepark West 1</dd>
    <dd>76863 Herxheim</dd>
    <dd><a href="https://goo.gl/maps/UXPhnXjS5X8vKnC38" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/mitschs_1457_imbiss/mitschs_1457_imbiss.jpg' | relative_url }} "Seite 1"){: .menu-card}



