---
layout: restaurant
categories: [restaurant]
title: Multi Imbiss
permalink: /multi_imbiss
specialities: [Italienisch, T&uuml;rkisch]
speciality: "Italienisch / T&uuml;rkisch"
village: Bellheim
deliver: true
openingHours:
 - { openHour: 13, openMinute: 0, closeHour: 22, closeMinute: 30 } # sunday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 }  # monday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # tuesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # thursday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 22, closeMinute: 30 } # saturday
 - { openHour: 13, openMinute: 0, closeHour: 22, closeMinute: 30  } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Multi Imbiss</dd>
    <dd>Tel: <a href="tel:+4972729724211">07272 9724211</a></dd>
    <dd>Hauptstraße 135</dd>
    <dd>76756 Bellheim</dd>
    <dd><a href="https://goo.gl/maps/W84g4VSB7x7TDWhg9" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/multi-imbiss/multi-imbiss-0.jpg' | relative_url }} "Seite 1"){: .menu-card}


