---
layout: restaurant
categories: [restaurant]
title: Schnitzelwerk
permalink: /schnitzelwerk
specialities: [Deutsch]
speciality: "Deutsch"
village: H&ouml;rdt
deliver: true
minimum_deliver_order: 25
openingHours:
 - { openHour: 12, openMinute: 0, closeHour: 20, closeMinute: 0 } # sunday
 - null                                                           # monday
 - null # tuesday
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 30 } # wednesday
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 30 } # thursday
 - { openHour: 12, openMinute: 00, closeHour: 20, closeMinute: 30 } # friday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 30 } # saturday
 - { openHour: 12, openMinute: 0, closeHour: 20, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Schnitzelwerk</dd>
    <dd>Tel: <a href="tel:+4972727709679">07272 7709679</a></dd>
    <dd>Kirchstraße 58</dd>
    <dd>76771 H&ouml;rdt</dd>
    <dd><a href="https://goo.gl/maps/do77K1SYr9Xmpxci6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/schnitzelwerk/schnitzelwerk-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

