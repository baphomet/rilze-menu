---
layout: restaurant
categories: [restaurant]
title: Thang Long
permalink: /thang-long
specialities: [Asiatisch]
speciality: "Asiatisch"
village: R&uuml;lzheim
deliver: false
openingHours:
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # sunday
 - null                                                           # monday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # tuesday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # wednesday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # thursday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 0, closeHour: 22, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Thang Long Asia Imbiss</dd>
    <dd>Tel: <a href="tel:+4972729328110">07272 9328110</a></dd>
    <dd>Max-Planck-Straße 8</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/DMaUfANvWWpzAHHL6" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/thang-long/thang-long-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/thang-long/thang-long-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/thang-long/thang-long-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/thang-long/thang-long-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/thang-long/thang-long-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/thang-long/thang-long-6.jpg' | relative_url }} "Seite 6"){: .menu-card}
