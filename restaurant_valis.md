---
layout: restaurant
categories: [restaurant]
title: Vali's Restaurant
permalink: /valis
specialities: [Deutsch]
speciality: "Deutsch"
village: Leimersheim
deliver: false
openingHours:
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # sunday
 - null                                                           # monday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # tuesday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # wednesday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # thursday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # friday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # saturday
 - { openHour: 12, openMinute: 0, closeHour: 23, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Vali's Restaurant</dd>
    <dd>Tel: <a href="tel:+4917662713588">01766 2713588</a></dd>
    <dd>St.Gertrudis-Straße 9</dd>
    <dd>776774 Leimersheim</dd>
    <dd><a href="https://goo.gl/maps/bdYCJknKKFnwQkyn8" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/valis/valis-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/valis/valis-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/valis/valis-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/valis/valis-4.jpg' | relative_url }} "Seite 4"){: .menu-card}
