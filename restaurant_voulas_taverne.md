---
layout: restaurant
categories: [restaurant]
title: Voulas Taverne
permalink: /voulas_taverne
specialities: [Griechisch]
speciality: "Griechisch"
village: Bellheim
deliver: true
openingHours:
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # sunday
 - null                                                            # monday
 - { openHour: 17, openMinute: 30, closeHour: 22, closeMinute: 0 } # tuesday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # wednesday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # thursday
 - { openHour: 17, openMinute: 00, closeHour: 22, closeMinute: 0 } # friday
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # saturday
 - { openHour: 11, openMinute: 00, closeHour: 22, closeMinute: 0 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Voulas Taverne</dd>
    <dd>Tel: <a href="tel:+4917641814846">0176 41814846</a></dd>
    <dd>Am Stockweg 1</dd>
    <dd>76756 Bellheim</dd>
    <dd><a href="https://g.page/voulastavernebellheim?share" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas      fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/voulas/voulas_taverne.jpg' | relative_url }} "Seite 1"){: .menu-card}
