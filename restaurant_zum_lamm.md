---
layout: restaurant
categories: [restaurant]
title: Zum Lamm
permalink: /zum-lamm
specialities: [Deutsch]
speciality: "Deutsch"
village: Neupotz
deliver: false
openingHours:
 - { openHour: 11, openMinute: 30, closeHour: 14, closeMinute: 30 } # sunday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 30 } # monday
 - null                                                             # tuesday
 - { openHour: 11, openMinute: 30, closeHour: 21, closeMinute: 30 } # wednesday
 - { openHour: 11, openMinute: 30, closeHour: 21, closeMinute: 30 } # thursday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 30 } # friday
 - { openHour: 11, openMinute: 30, closeHour: 21, closeMinute: 30 } # saturday
 - { openHour: 11, openMinute: 30, closeHour: 14, closeMinute: 30 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Zum Lamm</dd>
    <dd>Tel: <a href="tel:+4972722809">07272 2809</a></dd>
    <dd> Hauptstraße 7</dd>
    <dd>76777 Neupotz</dd>
    <dd><a href="https://goo.gl/maps/zrtHRGmcDXAydWv98" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/lamm/lamm-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/lamm/lamm-2.jpg' | relative_url }} "Seite 2"){: .menu-card}






