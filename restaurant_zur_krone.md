---
layout: restaurant
categories: [restaurant]
title: Zur Krone
permalink: /zur-krone
specialities: [Deutsch]
speciality: "Deutsch"
village: R&uuml;lzheim
deliver: false
openingHours:
 - null                                                             # sunday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # monday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # tuesday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # wednesday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # thursday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # friday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # saturday
 - { openHour: 17, openMinute: 30, closeHour: 21, closeMinute: 00 } # holiday
address: >
  <dl>
    <dt>Adresse</dt>
    <dd>Zur Krone</dd>
    <dd>Tel: <a href="tel:+4972728389">07272 8389</a></dd>
    <dd>Mittlere Ortsstraße 67</dd>
    <dd>76761 R&uuml;lzheim</dd>
    <dd><a href="https://goo.gl/maps/hWhK7vvGWrE9J3FE7" target="_blank" class="btn btn-warning btn-sm" role="button" aria-pressed="true"><i class="fas fa-map-marker-alt"></i>&nbsp;Google Maps</a></dd>
  </dl>
---
{%- include generateOpeningHours.js -%}

![Seite 1]({{ '/assets/images/restaurants/krone/krone-1.jpg' | relative_url }} "Seite 1"){: .menu-card}

![Seite 2]({{ '/assets/images/restaurants/krone/krone-2.jpg' | relative_url }} "Seite 2"){: .menu-card}

![Seite 3]({{ '/assets/images/restaurants/krone/krone-3.jpg' | relative_url }} "Seite 3"){: .menu-card}

![Seite 4]({{ '/assets/images/restaurants/krone/krone-4.jpg' | relative_url }} "Seite 4"){: .menu-card}

![Seite 5]({{ '/assets/images/restaurants/krone/krone-5.jpg' | relative_url }} "Seite 5"){: .menu-card}

![Seite 6]({{ '/assets/images/restaurants/krone/krone-6.jpg' | relative_url }} "Seite 6"){: .menu-card}

![Seite 7]({{ '/assets/images/restaurants/krone/krone-7.jpg' | relative_url }} "Seite 7"){: .menu-card}




