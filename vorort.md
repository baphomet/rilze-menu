---
layout: page
permalink: /vorort
description: > # this means to ignore newlines
  <strong>Speisekarten f&uuml;r Besuch oder Abholservice</strong><br />
  Hier findest du alle Speisekarten aus R&uuml;lzheim und der Umgebung<br />
description_title: "Besuch oder Abholservice"
---
<table class="table table-striped table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Restaurant</th>
      <th scope="col">Ort</th>
      <th scope="col">Spezialit&auml;t</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
{%- for page in site.pages -%}
  {%- if page.categories contains 'restaurant' -%}
    <tr>
        <td scope="row"><a href="{{ page.url | absolute_url }}" class="stretched-link">{{page.title}}</a></td>
        <td>{{page.village}}</td>
        <td>{{page.speciality}}</td>
        <td id="{{ page.path | slugify | replace: '-', '_' }}_openHoursCurrentState"></td>
    </tr>
  {%- endif -%}
{%- endfor -%}
  </tbody>
</table>

{%- include getCurrentOpeningHoursState.js -%}
<script>
function setOpeningHoursState(elementId, state) {
  document.getElementById(elementId).innerHTML = (state ? "Geöffnet!": "Geschlossen!");
  document.getElementById(elementId).style.color = (state ? "green" : "red");
}

var today = new Date();
var weekDay = today.getDay();

{%- for page in site.pages -%}
  {%- if page.categories contains 'restaurant' -%}
      {%- assign restaurant_name = page.path | slugify | replace: '-', '_'  -%}
      var {{ restaurant_name }}_openingHours = [ 
        {%- for day in page.openingHours -%}
        [
          {%- if day == null -%}
            null, null
          {%- else -%}
          new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.openHour }}, {{ day.openMinute }}, 0, 0),
          new Date(today.getFullYear(), today.getMonth(), today.getDate(), {{ day.closeHour }}, {{ day.closeMinute }}, 0, 0)
          {%- endif -%}
        ],
        {%- endfor -%}
      ];
      var {{ restaurant_name }}_state = getCurrentOpeningHoursState(today, {{ restaurant_name }}_openingHours[weekDay]);
      setOpeningHoursState("{{ restaurant_name }}_openHoursCurrentState", {{ restaurant_name }}_state);
  {%- endif -%}
{%- endfor -%}

